import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BurgerComponent } from './burger/burger.component';
import { IngredientsComponent } from './burger/ingredients/ingredients.component';

@NgModule({
  declarations: [
    AppComponent,
    BurgerComponent,
    IngredientsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
