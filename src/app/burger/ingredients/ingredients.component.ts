import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {

  @Input() meat =  '';
  @Input() cheese =  ''
  @Input() salad = '';
  @Input() bacon = '';
  @Output() delete = new EventEmitter();

  price(){
    if (this.meat){console.log(50)}
    if (this.cheese){console.log(20)}
    if (this.salad){console.log(5)}
    if (this.bacon){console.log(30)}

  }
  onClickDelete(){
    this.delete.emit()
  }

onDell(e: Event){
e.preventDefault()
}
}
